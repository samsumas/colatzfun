
template<unsigned n>
unsigned collatz_steps(unsigned akku) {
    if (n & 0x1) {
        return collatz_steps<(n/2)>(akku + 1);
    } else {
        return collatz_steps<((3*n+1)/2)>(akku + 2);
    }
}

template<>
unsigned collatz_steps<1>(unsigned akku) {
    return akku;
}

//lenny constexpr
constexpr unsigned long limit = 100'000'000;
constexpr unsigned long collatz(unsigned long n, unsigned long akku) {
    if(n == 1) {
        return akku;
    } else if (n % 2 == 0) {
        return collatz(n/2, akku + 1);
    } else {
        return collatz(3*n+1, akku + 1);
    }
}

constexpr unsigned long find_max_until(const unsigned long n) {
    unsigned long index = 0;
    unsigned long longest = 0;
    for(unsigned long i = 1; i < n; i++){
        auto c = collatz(i, 1);
        if(c > longest) {
            longest = c;
            index = i;
        }
    }
    return index;
}

constexpr void print_collatz(const unsigned long n) {
    auto x = n;
    while(x > 1) {
        printf("%lu, ", x);
        if (x % 2 == 0) {
            x /= 2;
        } else {
            x = 3 * x + 1;
        }
    }
    printf("%lu\n", x);
}